# README #

1/3スケールの富士通 FM-77風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- 富士通

## 発売時期

- FM-77D1/D2 1984年5月
- FM-77L4 1985年2月
- FM-77L2 1985年5月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/FM-7#FM-77)
- [ボクたちが愛した、想い出のパソコン・マイコンたち 富士通 FM-7の実質的な後継機種「FM-77」](https://akiba-pc.watch.impress.co.jp/docs/column/retrohard/1078210.html)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-77/raw/dd54f64b997ffc35ba986d95c9a060daf2125a51/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-77/raw/dd54f64b997ffc35ba986d95c9a060daf2125a51/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fm-77/raw/dd54f64b997ffc35ba986d95c9a060daf2125a51/ExampleImage.png)
